FROM nginx
LABEL MAINTAINER="Paulo Siqueira <pmp.paulosiqueira@gmail.com>"
COPY cloudedemais-desenv /usr/share/nginx/html
RUN apt-get update && apt-get install -y vim nano
WORKDIR /usr/share/nginx/html